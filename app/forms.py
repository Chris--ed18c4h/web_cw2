from flask_wtf import Form
from wtforms import IntegerField
from wtforms import TextField
from wtforms import DateField
from wtforms import BooleanField
from wtforms.validators import DataRequired

class BookForm(Form):
    title = TextField('Book Title', validators=[DataRequired()])
    author = TextField('Author', validators=[DataRequired()])
    genre = TextField('Genre', validators=[DataRequired()])
    publish = DateField('Published', format='%d-%m-%Y')
    pages = IntegerField("pages", validators=[DataRequired()])

class UserForm(Form):
    name = TextField('name', validators=[DataRequired()])
    age = TextField('age', validators=[DataRequired()])
    module_desc = TextField('Genre', validators=[DataRequired()])
    deadline = DateField('Published')
    is_complete = IntegerField("pages", validators=[DataRequired()])