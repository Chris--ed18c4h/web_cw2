from flask import render_template
from flask.helpers import flash
from app import app
from .forms import UserForm, BookForm


@app.route('/add_u', methods=['GET', 'POST'])
def add_u():	
    form = UserForm()
    if form.validate_on_submit():
        flash('Succesfully received form data. %s  %s   %s'%(form.name.data,form.age.data,form.deadline.data))
    return render_template('add_u.html',title = "add_user",form = form)

@app.route('/add_b', methods=['GET', 'POST'])
def add_b():	
    form = BookForm()
    if form.validate_on_submit():
        flash('Succesfully received form data. %s  %s   %s'%(form.title.data,form.author.data,form.publish.data))
    return render_template('add_b.html',title = "add_book",form = form)

@app.route('/')
def index():
    return render_template('welc.html')

